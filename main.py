import cv2
   
# Create a VideoCapture object and read from input file 
cap = cv2.VideoCapture('video.mp4')

# Read until video is completed 
while True: 
      
  # Capture frame-by-frame 
  ret, frame = cap.read()
  if ret:

    # Converting frame from BGR to HSV
    hsv_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

    # Creating a mask
    mask1 = cv2.inRange(hsv_frame, (1, 190, 200), (18, 255, 255))
    mask2 = cv2.inRange(hsv_frame, (0, 0, 200), (145, 60, 255))
    mask = mask1 + mask2

    # Imposing mask on top of the original frame
    result = cv2.bitwise_and(frame, frame, mask=mask)

    # Display the resulting frame 
    cv2.imshow('Original video', frame)
    cv2.imshow('Color segmentation', result)
    
    if cv2.waitKey(1) & 0xFF == ord('q'): 
      break
    
  else:  
   # cap.set(cv2.CAP_PROP_POS_FRAMES, 0)
   break

# When everything done, release  
# the video capture object 
cap.release()
   
# Closes all the frames 
cv2.destroyAllWindows()
